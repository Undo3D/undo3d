//// Base < App
//// Represents a single Undo3D application.

import Base from './base.mjs'

export default class App extends Base {
}

//// Static properties.
Object.defineProperties(App, {
    tree: { value:Base.tree+' < App', enumerable:true }
})
