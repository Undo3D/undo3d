//// Base
//// The base class for all other classes.

export default class Base {
}

//// Static properties.
Object.defineProperties(Base, {
    tree: { value:'Base', enumerable:true }
})
