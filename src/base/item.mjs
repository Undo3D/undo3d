//// Base < Item
//// Something listable: you can ‘B.R.E.A.D’ with it.

import Base from './base.mjs'

export default class Item extends Base {
}

//// Static properties.
Object.defineProperties(Item, {
    tree: { value:Base.tree+' < Item', enumerable:true }
})
