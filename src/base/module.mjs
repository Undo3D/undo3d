//// Base < Module
//// A discrete area of functionality, or ‘business logic’.

import Base from './base.mjs'

export default class Module extends Base {
}

//// Static properties.
Object.defineProperties(Module, {
    tree: { value:Base.tree+' < Module', enumerable:true }
})
