//// Base < Ui
//// A user-interface element.

import Base from './base.mjs'

export default class Ui extends Base {
}

//// Static properties.
Object.defineProperties(Ui, {
    tree: { value:Base.tree+' < Ui', enumerable:true }
})
