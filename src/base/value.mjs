//// Base < Value
//// An Item is composed of Values.

import Base from './base.mjs'

export default class Value extends Base {
}

//// Static properties.
Object.defineProperties(Value, {
    tree: { value:Base.tree+' < Value', enumerable:true }
})
