//// Base < Module < Boot
//// Manages app and Module lifecycles.

import Module from '../../base/module.mjs'

export default class Boot extends Module {
}

//// Static properties.
Object.defineProperties(Boot, {
    tree: { value:Module.tree+' < Boot', enumerable:true }
})
