//// Base < Module < Cli
//// Every app has a Command Line Interface - usually hidden.

import Module from '../../base/module.mjs'

export default class Cli extends Module {
}

//// Static properties.
Object.defineProperties(Cli, {
    tree: { value:Module.tree+' < Cli', enumerable:true }
})
