//// Base < Item < Logline
//// The record of a Cli command, or some other debug message.

import Item from '../../base/item.mjs'

export default class Logline extends Item {
}

//// Static properties.
Object.defineProperties(Logline, {
    tree: { value:Item.tree+' < Logline', enumerable:true }
})
