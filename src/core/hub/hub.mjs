//// Base < Module < Hub
//// The app’s nerve centre. Contains a list of Listeners.

import Module from '../../base/module.mjs'

export default class Hub extends Module {
}

//// Static properties.
Object.defineProperties(Hub, {
    tree: { value:Module.tree+' < Hub', enumerable:true }
})
