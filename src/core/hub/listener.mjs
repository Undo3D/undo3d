//// Base < Item < Listener
//// A stored function, fired by the Hub.

import Item from '../../base/item.mjs'

export default class Listener extends Item {
}

//// Static properties.
Object.defineProperties(Listener, {
    tree: { value:Item.tree+' < Listener', enumerable:true }
})
