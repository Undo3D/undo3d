//// Base < Item < Route
//// Routes link URLs to Zone and Panel visibilities.

import Item from '../../base/item.mjs'

export default class Route extends Item {
}

//// Static properties.
Object.defineProperties(Route, {
    tree: { value:Item.tree+' < Route', enumerable:true }
})
