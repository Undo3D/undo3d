//// Base < Module < Router
//// Manages the app’s internal navigation.

import Module from '../../base/module.mjs'

export default class Router extends Module {
}

//// Static properties.
Object.defineProperties(Router, {
    tree: { value:Module.tree+' < Router', enumerable:true }
})
