//// Base < Module < State
//// A List of Values, persisted in various ways.

import Module from '../../base/module.mjs'

export default class State extends Module {
}

//// Static properties.
Object.defineProperties(State, {
    tree: { value:Module.tree+' < State', enumerable:true }
})
