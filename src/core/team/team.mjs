//// Base < Module < Team
//// The app’s set of Users. Minimally, just ‘anonymous’.

import Module from '../../base/module.mjs'

export default class Team extends Module {
}

//// Static properties.
Object.defineProperties(Team, {
    tree: { value:Module.tree+' < Team', enumerable:true }
})
