//// Base < Item < User
//// Usually represents a person. Could be a robot though.

import Item from '../../base/item.mjs'

export default class User extends Item {
}

//// Static properties.
Object.defineProperties(User, {
    tree: { value:Item.tree+' < User', enumerable:true }
})
