//// Base < Ui < Form
//// Edits most Values of one Item - sometimes more than one.

import Ui from '../base/ui.mjs'

export default class Form extends Ui {
}

//// Static properties.
Object.defineProperties(Form, {
    tree: { value:Ui.tree+' < Form', enumerable:true }
})
