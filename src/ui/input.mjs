//// Base < Ui < Input
//// Edits one Value - sometimes more than one.

import Ui from '../base/ui.mjs'

export default class Input extends Ui {
}

//// Static properties.
Object.defineProperties(Input, {
    tree: { value:Ui.tree+' < Input', enumerable:true }
})
