//// Base < Ui < List
//// An ordered sequence of Items, usually of the same type.

import Ui from '../base/ui.mjs'

export default class List extends Ui {
}

//// Static properties.
Object.defineProperties(List, {
    tree: { value:Ui.tree+' < List', enumerable:true }
})
