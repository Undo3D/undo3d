//// Base < Ui < Panel
//// A control panel, often for a Module.

import Ui from '../base/ui.mjs'

export default class Panel extends Ui {
}

//// Static properties.
Object.defineProperties(Panel, {
    tree: { value:Ui.tree+' < Panel', enumerable:true }
})
