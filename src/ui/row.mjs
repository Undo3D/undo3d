//// Base < Ui < Row
//// Edits a few Values of one Item.

import Ui from '../base/ui.mjs'

export default class Row extends Ui {
}

//// Static properties.
Object.defineProperties(Row, {
    tree: { value:Ui.tree+' < Row', enumerable:true }
})
