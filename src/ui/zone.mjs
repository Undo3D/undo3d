//// Base < Ui < Zone
//// A section of the screen, containing Panels.

import Ui from '../base/ui.mjs'

export default class Zone extends Ui {
}

//// Static properties.
Object.defineProperties(Zone, {
    tree: { value:Ui.tree+' < Zone', enumerable:true }
})
