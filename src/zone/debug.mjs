//// Base < Ui < Zone < Debug
//// Contains helpful developer tools. For example the Cli.

import Zone from '../ui/zone.mjs'

export default class Debug extends Zone {
}

//// Static properties.
Object.defineProperties(Debug, {
    tree: { value:Zone.tree+' < Debug', enumerable:true }
})
