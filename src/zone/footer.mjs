//// Base < Ui < Zone < Footer
//// Small, fullwidth, and at the end. Partly or fully sticky.

import Zone from '../ui/zone.mjs'

export default class Footer extends Zone {
}

//// Static properties.
Object.defineProperties(Footer, {
    tree: { value:Zone.tree+' < Footer', enumerable:true }
})
