//// Base < Ui < Zone < Header
//// Small, fullwidth, and up top. Partly or fully sticky.

import Zone from '../ui/zone.mjs'

export default class Header extends Zone {
}

//// Static properties.
Object.defineProperties(Header, {
    tree: { value:Zone.tree+' < Header', enumerable:true }
})
