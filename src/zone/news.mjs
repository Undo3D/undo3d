//// Base < Ui < Zone < News
//// Usually a dismissable list of notifications.

import Zone from '../ui/zone.mjs'

export default class News extends Zone {
}

//// Static properties.
Object.defineProperties(News, {
    tree: { value:Zone.tree+' < News', enumerable:true }
})
