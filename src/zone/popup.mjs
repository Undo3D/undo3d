//// Base < Ui < Zone < Popup
//// A dialog or alert. Temporarily prevents UI interactions.

import Zone from '../ui/zone.mjs'

export default class Popup extends Zone {
}

//// Static properties.
Object.defineProperties(Popup, {
    tree: { value:Zone.tree+' < Popup', enumerable:true }
})
