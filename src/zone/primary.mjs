//// Base < Ui < Zone < Primary
//// Contains the main content, often a 3D scene.

import Zone from '../ui/zone.mjs'

export default class Primary extends Zone {
}

//// Static properties.
Object.defineProperties(Primary, {
    tree: { value:Zone.tree+' < Primary', enumerable:true }
})
