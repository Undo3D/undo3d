//// Base < Ui < Zone < Secondary
//// The preferred place to display Forms.

import Zone from '../ui/zone.mjs'

export default class Secondary extends Zone {
}

//// Static properties.
Object.defineProperties(Secondary, {
    tree: { value:Zone.tree+' < Secondary', enumerable:true }
})
