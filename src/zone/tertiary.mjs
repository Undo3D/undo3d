//// Base < Ui < Zone < Tertiary
//// The preferred place to display Lists.

import Zone from '../ui/zone.mjs'

export default class Tertiary extends Zone {
}

//// Static properties.
Object.defineProperties(Tertiary, {
    tree: { value:Zone.tree+' < Tertiary', enumerable:true }
})
