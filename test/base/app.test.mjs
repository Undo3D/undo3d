//// Unit test for Base < App
//// Represents a single Undo3D application.

import App from '../../src/base/app.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < App', App.tree,
    `App.tree should be 'Base < App'`)
