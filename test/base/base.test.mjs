//// Unit test for Base
//// The base class for all other classes.

import Base from '../../src/base/base.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base', Base.tree,
    `Base.tree should be 'Base'`)
