//// Unit test for Base < Item
//// Something listable: you can ‘B.R.E.A.D’ with it.

import Item from '../../src/base/item.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Item', Item.tree,
    `Item.tree should be 'Base < Item'`)
