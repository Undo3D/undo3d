//// Unit test for Base < Module
//// A discrete area of functionality, or ‘business logic’.

import Module from '../../src/base/module.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Module', Module.tree,
    `Module.tree should be 'Base < Module'`)
