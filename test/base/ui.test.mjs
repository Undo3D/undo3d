//// Unit test for Base < Ui
//// A user-interface element.

import Ui from '../../src/base/ui.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui', Ui.tree,
    `Ui.tree should be 'Base < Ui'`)
