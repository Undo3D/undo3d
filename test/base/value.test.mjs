//// Unit test for Base < Value
//// An Item is composed of Values.

import Value from '../../src/base/value.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Value', Value.tree,
    `Value.tree should be 'Base < Value'`)
