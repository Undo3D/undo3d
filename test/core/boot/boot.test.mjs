//// Unit test for Base < Module < Boot
//// Manages app and Module lifecycles.

import Boot from '../../../src/core/boot/boot.mjs'
import { strict as a } from '../../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Module < Boot', Boot.tree,
    `Boot.tree should be 'Base < Module < Boot'`)
