//// Unit test for Base < Module < Cli
//// Every app has a Command Line Interface - usually hidden.

import Cli from '../../../src/core/cli/cli.mjs'
import { strict as a } from '../../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Module < Cli', Cli.tree,
    `Cli.tree should be 'Base < Module < Cli'`)
