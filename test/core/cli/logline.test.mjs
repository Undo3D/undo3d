//// Unit test for Base < Item < Logline
//// The record of a Cli command, or some other debug message.

import Logline from '../../../src/core/cli/logline.mjs'
import { strict as a } from '../../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Item < Logline', Logline.tree,
    `Logline.tree should be 'Base < Item < Logline'`)
