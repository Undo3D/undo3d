//// Unit test for Base < Module < Hub
//// The app’s nerve centre. Contains a list of Listeners.

import Hub from '../../../src/core/hub/hub.mjs'
import { strict as a } from '../../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Module < Hub', Hub.tree,
    `Hub.tree should be 'Base < Module < Hub'`)
