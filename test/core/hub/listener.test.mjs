//// Unit test for Base < Item < Listener
//// A stored function, fired by the Hub.

import Listener from '../../../src/core/hub/listener.mjs'
import { strict as a } from '../../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Item < Listener', Listener.tree,
    `Listener.tree should be 'Base < Item < Listener'`)
