//// Unit test for Base < Item < Route
//// Routes link URLs to Zone and Panel visibilities.

import Route from '../../../src/core/router/route.mjs'
import { strict as a } from '../../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Item < Route', Route.tree,
    `Route.tree should be 'Base < Item < Route'`)
