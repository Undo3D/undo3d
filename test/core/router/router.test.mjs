//// Unit test for Base < Module < Router
//// Manages the app’s internal navigation.

import Router from '../../../src/core/router/router.mjs'
import { strict as a } from '../../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Module < Router', Router.tree,
    `Router.tree should be 'Base < Module < Router'`)
