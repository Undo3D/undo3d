//// Unit test for Base < Module < State
//// A List of Values, persisted in various ways.

import State from '../../../src/core/state/state.mjs'
import { strict as a } from '../../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Module < State', State.tree,
    `State.tree should be 'Base < Module < State'`)
