//// Unit test for Base < Module < Team
//// The app’s set of Users. Minimally, just ‘anonymous’.

import Team from '../../../src/core/team/team.mjs'
import { strict as a } from '../../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Module < Team', Team.tree,
    `Team.tree should be 'Base < Module < Team'`)
