//// Unit test for Base < Item < User
//// Usually represents a person. Could be a robot though.

import User from '../../../src/core/team/user.mjs'
import { strict as a } from '../../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Item < User', User.tree,
    `User.tree should be 'Base < Item < User'`)
