//// Unit test for Base < Ui < Form
//// Edits most Values of one Item - sometimes more than one.

import Form from '../../src/ui/form.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Form', Form.tree,
    `Form.tree should be 'Base < Ui < Form'`)
