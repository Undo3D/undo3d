//// Unit test for Base < Ui < Input
//// Edits one Value - sometimes more than one.

import Input from '../../src/ui/input.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Input', Input.tree,
    `Input.tree should be 'Base < Ui < Input'`)
