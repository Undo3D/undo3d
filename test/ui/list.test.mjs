//// Unit test for Base < Ui < List
//// An ordered sequence of Items, usually of the same type.

import List from '../../src/ui/list.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < List', List.tree,
    `List.tree should be 'Base < Ui < List'`)
