//// Unit test for Base < Ui < Panel
//// A control panel, often for a Module.

import Panel from '../../src/ui/panel.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Panel', Panel.tree,
    `Panel.tree should be 'Base < Ui < Panel'`)
