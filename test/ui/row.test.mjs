//// Unit test for Base < Ui < Row
//// Edits a few Values of one Item.

import Row from '../../src/ui/row.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Row', Row.tree,
    `Row.tree should be 'Base < Ui < Row'`)
