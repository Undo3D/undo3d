//// Unit test for Base < Ui < Zone
//// A section of the screen, containing Panels.

import Zone from '../../src/ui/zone.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Zone', Zone.tree,
    `Zone.tree should be 'Base < Ui < Zone'`)
