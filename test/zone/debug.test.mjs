//// Unit test for Base < Ui < Zone < Debug
//// Contains helpful developer tools. For example the Cli.

import Debug from '../../src/zone/debug.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Zone < Debug', Debug.tree,
    `Debug.tree should be 'Base < Ui < Zone < Debug'`)
