//// Unit test for Base < Ui < Zone < Footer
//// Small, fullwidth, and at the end. Partly or fully sticky.

import Footer from '../../src/zone/footer.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Zone < Footer', Footer.tree,
    `Footer.tree should be 'Base < Ui < Zone < Footer'`)
