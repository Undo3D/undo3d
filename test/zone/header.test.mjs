//// Unit test for Base < Ui < Zone < Header
//// Small, fullwidth, and up top. Partly or fully sticky.

import Header from '../../src/zone/header.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Zone < Header', Header.tree,
    `Header.tree should be 'Base < Ui < Zone < Header'`)
