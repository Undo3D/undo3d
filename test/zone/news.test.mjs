//// Unit test for Base < Ui < Zone < News
//// Usually a dismissable list of notifications.

import News from '../../src/zone/news.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Zone < News', News.tree,
    `News.tree should be 'Base < Ui < Zone < News'`)
