//// Unit test for Base < Ui < Zone < Popup
//// A dialog or alert. Temporarily prevents UI interactions.

import Popup from '../../src/zone/popup.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Zone < Popup', Popup.tree,
    `Popup.tree should be 'Base < Ui < Zone < Popup'`)
