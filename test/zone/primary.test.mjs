//// Unit test for Base < Ui < Zone < Primary
//// Contains the main content, often a 3D scene.

import Primary from '../../src/zone/primary.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Zone < Primary', Primary.tree,
    `Primary.tree should be 'Base < Ui < Zone < Primary'`)
