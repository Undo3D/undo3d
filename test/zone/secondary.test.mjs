//// Unit test for Base < Ui < Zone < Secondary
//// The preferred place to display Forms.

import Secondary from '../../src/zone/secondary.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Zone < Secondary', Secondary.tree,
    `Secondary.tree should be 'Base < Ui < Zone < Secondary'`)
