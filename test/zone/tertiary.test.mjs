//// Unit test for Base < Ui < Zone < Tertiary
//// The preferred place to display Lists.

import Tertiary from '../../src/zone/tertiary.mjs'
import { strict as a } from '../../deps/node_modules/undo3d-shim-browser/assert/all.mjs'

a.equal('Base < Ui < Zone < Tertiary', Tertiary.tree,
    `Tertiary.tree should be 'Base < Ui < Zone < Tertiary'`)
